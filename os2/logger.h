#pragma once
#include <string>
#include <iostream>


using namespace std;

class Logger
{
	static bool debugConsole;
	static FILE * logFile;

public :
	static void log(string msg);
};