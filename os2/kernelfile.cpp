#include "kernelfile.h"

#include <iostream>


#define DEBUG_ROLS 0

using namespace std;

KernelFile::KernelFile(string fileName, bool write, KernelPartition* k, ClusterNo clusterNo, unsigned long size)
{
	if (DEBUG_ROLS) Logger::log("   KernelFile constructor");

	name = fileName;
	writePermission = write;
	mutex = FS::myImpl ->mutex;
	kernelPartition = k;
	indexCluster = clusterNo;
	//indexBuffer = indexBuff;
	currentSize = size;
	cursor = 0;

	fillClustersFromIndex();

	validCurrentCluster = false;
}



KernelFile::~KernelFile()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile destructor ");

	// destroy open file

	fillIndexFromClusters();

	if (writePermission)
	{
		kernelPartition->persistCache(indexCluster);

		for (ClusterNo clusterEntry : clustersDirty)
		{
			kernelPartition->persistCache(clusterEntry);
		}

		/*for (ClusterNo clusterEntry : secondLevelClustersDirty)
		{
			kernelPartition->persistCache(clusterEntry);
		}*/

		//for (ClusterNo clusterEntry : secondLevelIndexDirty)
		for (pair<ClusterNo,vector<ClusterNo>*> clusterEntry : secondLevelIndexClusterCache)
		{
			storeSecondLevelIndex(clusterEntry.first);
			kernelPartition->persistCache(clusterEntry.first);
		}
	}

	kernelPartition->setNewFileSize(name, currentSize);

	kernelPartition->closeFile(name, writePermission);
	
}

void KernelFile::lock()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile lock " + name);

	DWORD dwWaitResult = WaitForSingleObject(
		mutex,    // handle to mutex
		INFINITE);  // no time-out interval

	if (DEBUG_ROLS) Logger::log("   KernelFile locked " + name);
}

void KernelFile::unlock()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile unlock " + name);

	if (!ReleaseSemaphore(mutex,1,NULL))
	{
		// Handle error.
	}
}

char KernelFile::write(BytesCnt bytesCnt, char* buffer)
{
	if (DEBUG_ROLS) Logger::log("   KernelFile write " + name);

	if (!writePermission)
	{
		if (DEBUG_ROLS) Logger::log("Not allowed !");
		return 0;
	}

	unsigned long bufferCursor = 0;
	

	if (cursor % ClusterSize == 0)
	{
		validCurrentCluster = false;
	}

	if (!validCurrentCluster)
	{
		currentCluster = getClusterFromPosition(cursor);
		currentBuffer = kernelPartition->getCache(currentCluster);
		validCurrentCluster = true;
	}


	while (cursor < currentSize && bufferCursor < bytesCnt)
	{
		if (cursor % ClusterSize == 0)
		{
			currentCluster = getClusterFromPosition(cursor); //also adding to dirty clusters
			currentBuffer = kernelPartition->getCache(currentCluster);
		}

		currentBuffer[cursor++%ClusterSize] = buffer[bufferCursor++];
	}

	while (bufferCursor < bytesCnt)
	{
		if (cursor % ClusterSize == 0)
		{
			if (cursor < ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE)
			{
				//allocate first level

				if (DEBUG_ROLS) Logger::log(" allocate new  data cluster");
				currentCluster = kernelPartition->findAndTakeFreeCluster();
				if (currentCluster == NO_FREE_CLUSTERS)
				{
					if (DEBUG_ROLS) Logger::log(" no more space !");
					return 0;
				}
				fileClusters.push_back(currentCluster);
				//clustersCache[freeCluster] = kernelPartition->getCache(freeCluster);
				currentBuffer = kernelPartition->getEmptyCache(currentCluster);
				clustersDirty.insert(currentCluster);

			}
			//wont fit in ulong
			//else if (cursor < ClusterSize * FIRST_LEVEL_INDEX + ClusterSize*ClusterSize * (ClusterSize - FIRST_LEVEL_INDEX))
			//ou maj god, ju ar dam
			//else if (cursor <= ~(unsigned long)0)
			else if (cursor < ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE + 
				ClusterSize * ClusterSize/ENTRYSIZE * (ClusterSize - FIRST_LEVEL_INDEX)/ENTRYSIZE)
			{
				if ((cursor - ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE) % (ClusterSize*ClusterSize/ENTRYSIZE) == 0)
				{
					//allocate new second level index
					if (DEBUG_ROLS) Logger::log(" allocate new second level index");
					ClusterNo freeCluster = kernelPartition->findAndTakeFreeCluster();
					if (freeCluster == NO_FREE_CLUSTERS)
					{
						if (DEBUG_ROLS) Logger::log(" no more space !");
						return 0;
					}
					//char *buf = kernelPartition->getEmptyCache(freeCluster);
					kernelPartition->getEmptyCache(freeCluster);
					
					ClusterNo secondLevelInd = (cursor - ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE) / (ClusterSize*ClusterSize / ENTRYSIZE);
					//secondLevelIndexBuffer[secondLevelInd] = buf;
					//secondLevelIndexDirty.insert(freeCluster);
					secondLevelIndexClusterCache[freeCluster] = new vector<ClusterNo>();
					secondLevelIndexes.push_back(freeCluster);
				}

				//allocate new data cluster
				if (DEBUG_ROLS) Logger::log(" allocate new data cluster for second level");
				currentCluster = kernelPartition->findAndTakeFreeCluster();
				if (currentCluster == NO_FREE_CLUSTERS)
				{
					if (DEBUG_ROLS) Logger::log(" no more space !");
					return 0;
				}
				//ClusterNo secondLevelInd = (cursor - ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE) / (ClusterSize*ClusterSize / ENTRYSIZE);
				
				ClusterNo lastSecondLevelInd = secondLevelIndexes.back();
				
				secondLevelIndexClusterCache[lastSecondLevelInd]->push_back(currentCluster);
				//clustersCache[freeCluster] = kernelPartition->getCache(freeCluster);
				currentBuffer = kernelPartition->getEmptyCache(currentCluster);
				//secondLevelClustersDirty.insert(currentCluster);
				clustersDirty.insert(currentCluster);
			}
			else
			{
				if (DEBUG_ROLS) Logger::log(" no more space ! full file ");
				return 0;
			}
			
		}

		//kernelPartition->getCache(fileClusters.back())[cursor++%ClusterSize] = buffer[bufferCursor++];
		currentBuffer[cursor++%ClusterSize] = buffer[bufferCursor++];
		currentSize++;
	}

	return 1;
}


BytesCnt KernelFile::read(BytesCnt bytesCnt, char* buffer)
{
	if (DEBUG_ROLS) Logger::log("   KernelFile read " + name);

	//handle some errors
	//second level

	unsigned long bufferCursor = 0;

	//ugly
	if (eof())
		return 0;

	if (cursor % ClusterSize == 0)
	{
		validCurrentCluster = false;
	}

	if (!validCurrentCluster)
	{
		lock();
		currentCluster = getClusterFromPosition(cursor);
		currentBuffer = kernelPartition->getCache(currentCluster);
		validCurrentCluster = true;
		unlock();
	}


	while (cursor < currentSize && bufferCursor < bytesCnt)
	{
		if (cursor % ClusterSize == 0)
		{
			lock();
			currentCluster = getClusterFromPosition(cursor);

			currentBuffer = kernelPartition->getCache(currentCluster);
			unlock();
		}

		buffer[bufferCursor++] = currentBuffer[cursor++%ClusterSize];
	}

	return bufferCursor;
}


char KernelFile::seek(BytesCnt bytesCnt)
{
	if (DEBUG_ROLS) Logger::log("   KernelFile seek " + name);

	validCurrentCluster = false;

	cursor = bytesCnt;

	return bytesCnt;
}


BytesCnt KernelFile::filePos()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile filePos ");

	return cursor;
}


char KernelFile::eof()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile eof " + name);

	//handle errors;

	if (cursor == currentSize)
		return 2;
	return 0;
}


BytesCnt KernelFile::getFileSize()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile getFileSize ");

	return currentSize;
}


char KernelFile::truncate()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile truncate " + name);

	validCurrentCluster = false;

	if (!writePermission)
	{
		if (DEBUG_ROLS) Logger::log(" Not allowed !");
		return 0;
	}

	int currClusterCursor = getClusterFromPosition(cursor);

	for (unsigned ind = secondLevelIndexes.size(); ind > 0 ; ind--)
	{
		int currCluster = getClusterFromPosition(currentSize);

		if (currCluster != currClusterCursor)
		{
			if (DEBUG_ROLS) Logger::log(" releasing first level from second level");
			kernelPartition->releaseCluster(currCluster);
			fileClusters.pop_back();
			//secondLevelClustersDirty.erase(currCluster);
			clustersDirty.insert(currCluster);

			currentSize--;
			currentSize /= ClusterSize;
			currentSize *= ClusterSize;

			if ((currentSize - ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE) % (ClusterSize*ClusterSize / ENTRYSIZE) == 0)
			{
				if (DEBUG_ROLS) Logger::log(" releasing second level");

				int secondLevelInd = secondLevelIndexes.back();

				secondLevelIndexes.pop_back();
				secondLevelIndexClusterCache.erase(secondLevelInd);
				//delete secondLevelIndexBuffer[secondLevelInd];
				//secondLevelIndexDirty.erase(secondLevelInd);

				kernelPartition->releaseCluster(secondLevelInd);
			}
			else
			{
				//update first index vector in second index

				int secondLevelInd = secondLevelIndexes.back();

				secondLevelIndexClusterCache[secondLevelInd]->pop_back();
			}
		}
		else break;
	}

	for (unsigned ind = fileClusters.size(); ind > 0; ind--)
	{
		int currCluster = getClusterFromPosition(currentSize);

		if (currCluster != currClusterCursor)
		{
			if (DEBUG_ROLS) Logger::log(" releasing first level");
			kernelPartition->releaseCluster(currCluster);
			fileClusters.pop_back();
			clustersDirty.erase(currCluster);

			currentSize--;
			currentSize /= ClusterSize;
			currentSize *= ClusterSize;
		}
		else break;
	}

	currentSize = cursor;

	return 0;
}

ClusterNo KernelFile::getClusterFromPosition(unsigned long pos)
{
	if (DEBUG_ROLS) Logger::log("  KernelFile getClusterPosition");

	//hmmmm
	//if (pos == 0) return 0;

	if (pos < ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE)
	{
		//hmmmmmmmmmmmmmmmm
		if (fileClusters.size() <= (pos / ClusterSize))
		{
			return 0;
		}
		ClusterNo currentCluster = fileClusters[pos / ClusterSize];
		//if (clustersDirty.find(currentCluster) == clustersDirty.end())
		//{
			clustersDirty.insert(currentCluster);
			//clustersCache[clusterNumber] = kernelPartition->getCache(clusterNumber);
		//}
		return currentCluster;
	}
	else if (pos < ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE + ClusterSize*ClusterSize / ENTRYSIZE * (ClusterSize - FIRST_LEVEL_INDEX) / ENTRYSIZE)
	{
		

		int secondLevelInd = (pos - ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE) / (ClusterSize*ClusterSize / ENTRYSIZE);

		//hmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
		if (secondLevelIndexes.size() <= secondLevelInd )
		{
			return 0;
		}

		ClusterNo secondLevelIndReal = secondLevelIndexes[secondLevelInd];

		loadSecondLevelIndex(secondLevelIndReal);

		int secondLevelClusterInd = (pos - ClusterSize * FIRST_LEVEL_INDEX / ENTRYSIZE) % (ClusterSize*ClusterSize / ENTRYSIZE) / ClusterSize;

		if (secondLevelIndexClusterCache[secondLevelIndReal]->size() <= secondLevelClusterInd)
		{
			return 0;
		}

		ClusterNo currentCluster = (*secondLevelIndexClusterCache[secondLevelIndReal])[secondLevelClusterInd];

		//if (secondLevelClustersDirty.find(currentCluster) == secondLevelClustersDirty.end())
		//if (clustersDirty.find(currentCluster) == clustersDirty.end())
		//{
			clustersDirty.insert(currentCluster);
			//clustersCache[clusterNumber] = kernelPartition->getCache(clusterNumber);
		//}

		return currentCluster;
	}
}


void KernelFile::loadSecondLevelIndex(ClusterNo clusterNum)
{
	if (DEBUG_ROLS) Logger::log("   KernelFile loadSecondLevel");

	if (secondLevelIndexClusterCache.find(clusterNum) != secondLevelIndexClusterCache.end())
	{
		//already cached
		if (DEBUG_ROLS) Logger::log(" already cached");
		return;
	}

	secondLevelIndexClusterCache[clusterNum] = new vector<ClusterNo>();

	//ClusterNo clusterNumber = secondLevelIndexes[clusterNum];

	char *buf = kernelPartition->getCache(clusterNum);

	//secondLevelIndexBuffer[clusterNum] = buf;
	//secondLevelIndexDirty.insert(clusterNum);

	Seek seek(buf);
	seek.moveCursor(0);

	while (true)
	{
		if (seek.end())
			break;

		ClusterNo currDataCluster = seek.readULong();

		if (currDataCluster == 0)
			break;

		secondLevelIndexClusterCache[clusterNum]->push_back(currDataCluster);
	}
}

void KernelFile::storeSecondLevelIndex(ClusterNo clusterNum)
{
	if (DEBUG_ROLS) Logger::log("   KernelFile storeSecondLevel");

	//oh god, what a great naming.
	//ClusterNo clusterNumber = secondLevelIndexes[clusterNum];

	char *buf = kernelPartition->getCache(clusterNum);

	Seek seek(buf);
	seek.moveCursor(0);

	for (ClusterNo clusterNum : *secondLevelIndexClusterCache[clusterNum])
	{
		seek.writeULong(clusterNum);
	}
}


void KernelFile::fillClustersFromIndex()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile fillClusters");

	Seek seek(kernelPartition->getCache(indexCluster));
	seek.moveCursor(0);

	while (true)
	{
		if (seek.end())
			break;

		ClusterNo currDataCluster = seek.readULong();

		if (currDataCluster == 0)
			break;

		if (fileClusters.size() < FIRST_LEVEL_INDEX / ENTRYSIZE)
			fileClusters.push_back(currDataCluster);
		else
			secondLevelIndexes.push_back(currDataCluster);
	}

}

void KernelFile::fillIndexFromClusters()
{
	if (DEBUG_ROLS) Logger::log("   KernelFile fillindex");

	Seek seek(kernelPartition->getCache(indexCluster));
	seek.moveCursor(0);

	for (ClusterNo clusterNum : fileClusters)
	{
		seek.writeULong(clusterNum);
	}

	for (ClusterNo clusterNum : secondLevelIndexes)
	{
		seek.writeULong(clusterNum);
	}

}