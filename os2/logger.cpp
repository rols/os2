#include "logger.h"
#include <thread>

bool Logger::debugConsole = false;
FILE * Logger::logFile  = fopen("log.txt", "w");

void Logger::log(string msg)
{
	fprintf(logFile, "%d %s\n", this_thread::get_id(), msg.c_str());
	if (debugConsole)
		printf("%d %s\n", this_thread::get_id(), msg.c_str());
}
