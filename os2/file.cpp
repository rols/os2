#include "file.h"

File::File()
{
	
}

File::~File()
{
	delete myImpl;
}

char File::write(BytesCnt bytesCnt, char* buffer) 
{
	myImpl->lock();
	char ret = myImpl->write(bytesCnt, buffer);
	myImpl->unlock();
	return ret;
}


BytesCnt File::read(BytesCnt bytesCnt, char* buffer)
{
	//myImpl->lock();
	BytesCnt ret = myImpl->read(bytesCnt, buffer);
	//myImpl->unlock();
	return ret;
}


char File::seek(BytesCnt bytesCnt)
{
	//myImpl->lock();
	char ret = myImpl->seek(bytesCnt);
	//myImpl->unlock();
	return ret;
}


BytesCnt File::filePos()
{
	//myImpl->lock();
	BytesCnt ret = myImpl->filePos();
	//myImpl->unlock();
	return ret;
}


char File::eof()
{
	//myImpl->lock();
	char ret = myImpl->eof();
	//myImpl->unlock();
	return ret;
}


BytesCnt File::getFileSize()
{
	//myImpl->lock();
	BytesCnt ret = myImpl->getFileSize();
	//myImpl->unlock();
	return ret;
}


char File::truncate()
{
	myImpl->lock();
	char ret = myImpl->truncate();
	myImpl->unlock();
	return ret;
}