
#include"fs.h"
#include"file.h"
#include"part.h"


#include <cstdio>

#include <windows.h>
#include <string>
#include <iostream>
#include <string>

using namespace std;

string ExePath() {
	char buffer[MAX_PATH];
	//GetModuleFileName(NULL, buffer, MAX_PATH);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer).substr(0, pos);
}

void printCurrentDir()
{
	wchar_t buffer[MAX_PATH];
	GetCurrentDirectoryW(MAX_PATH, buffer);
	wstring ws(buffer);
	string str(ws.begin(), ws.end());
	cout << "Current working directory: " << str << endl;
}

int someTest()
{
	char buffer[20] = "lasjdnfljs";

	buffer[0] = 1;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;

	int sth;

	memcpy(&sth, buffer, 4);

	cout << "first int " << sth << endl;


	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 1;


	memcpy(&sth, buffer, 4);

	cout << "second int " << sth << endl;


	buffer[0] = 'a';
	buffer[1] = 'b';
	buffer[2] = 'c';
	buffer[3] = 0;

	string something(buffer);

	cout << something << " size " << something.length() << endl;
}


void anotherTest()
{
	char buffer[ClusterSize];

	buffer[125] = 5;

	Partition *partition1 = new Partition("p1.ini");

	partition1->writeCluster(0, buffer);

	cout << (int)buffer[125] << endl;
	buffer[125] = 1;

	partition1->readCluster(0, buffer);

	cout << (int)buffer[125] << endl;
}


void mutexTest()
{
	HANDLE nesto = CreateSemaphore(NULL, 1, 10000, NULL);
	cout << nesto;
	cout << WaitForSingleObject(nesto, INFINITE);
	cout << WaitForSingleObject(nesto, INFINITE);
	cout << WaitForSingleObject(nesto, INFINITE);
}

int main()
{
	//mutexTest();

	//someTest();

	//anotherTest(); return 0;

	//freopen("log.txt", "w", stdout);

	Partition* partition1 = new Partition("p1.ini");
	cout << "Success in creating a partition from p1.ini!" << endl << endl;

	char part1Ch = FS::mount(partition1);
	cout << "Success in mounting a partition with char " << part1Ch << "!" << endl << endl;

	FS::format(part1Ch);
	cout << "Success in formating a partition with char " << part1Ch << "!" << endl << endl;

	char path[20] = "1:/test.txt";
	path[0] = part1Ch;
	char path1[20] = "1:/cane.txt";
	path1[0] = part1Ch;
	char path2[20] = "1:/dimi.txt";
	path2[0] = part1Ch;

	cout << "DoesExist returns " << (int)FS::doesExist(path) << endl << endl;

	File *nesto = FS::open(path, 'w');
	cout << "fetched file !" << endl << endl;

	File *nesto1 = FS::open(path1, 'r');
	cout << "fetched file !" << endl << endl;

	File *nesto2 = FS::open(path2, 'a');
	cout << "fetched file !" << endl << endl;

	cout << "DoesExist returns " << (int)FS::doesExist(path) << endl << endl;

	Entry dir[64];

	int numEntries = FS::readRootDir(part1Ch, 0, dir);

	cout << "Found " << numEntries << " entries" << endl;
	for (int i = 0; i < numEntries; i++)
		cout << dir[i].getFullName() << endl;
	cout << endl;

	numEntries = FS::readRootDir(part1Ch, 1, dir);

	cout << "Found " << numEntries << " entries from 1 entry" << endl;
	for (int i = 0; i < numEntries; i++)
		cout << dir[i].getFullName() << endl;
	cout << endl;

	delete nesto;
	delete nesto1;

	FS::deleteFile(path);
	cout << "Deleted file " << path << endl << endl;

	numEntries = FS::readRootDir(part1Ch, 0, dir);

	cout << "Found " << numEntries << " entries" << endl;
	for (int i = 0; i < numEntries; i++)
		cout << dir[i].getFullName() << endl;
	cout << endl;

	char buffer[10] = "herheher";
	int ret = nesto2->write(5, buffer);
	cout << "Written to file " << path2 << " ret " << ret << endl << endl;

	memset(buffer, 0, 10);

	cout << "Read from file " << path2 << " ret " << (int)nesto2->read(5, buffer) << endl << endl;

	cout << "seek to 0 " << path2 << " ret " << (int)nesto2->seek(0) << endl << endl;

	cout << "Read from file " << path2 << " ret " << (int)nesto2->read(5, buffer) << endl;
	cout << buffer << endl << endl;

	delete nesto2;
	cout << "Close file " << path2 << endl << endl;

	nesto2 = FS::open(path2, 'a');
	cout << "fetched file !" << endl << endl;

	memset(buffer, 0, 10);
	cout << "Read from file " << path2 << " ret " << (int)nesto2->read(5, buffer) << endl;
	cout << buffer << endl << endl;

	nesto2->seek(0);
	memset(buffer, 0, 10);
	cout << "Read from file " << path2 << " ret " << (int)nesto2->read(5, buffer) << endl;
	cout << buffer << endl << endl;

	//check truncate
	cout << "--------------------------------------------" << endl << endl;
	nesto2->seek(3);
	cout << "truncate from file " << path2 << " ret " << (int)nesto2->truncate() << endl << endl;

	delete nesto2;
	cout << "Close file " << path2 << endl << endl;

	nesto2 = FS::open(path2, 'r');
	cout << "fetched file !" << endl << endl;

	memset(buffer, 0, 10);
	cout << "Read from file " << path2 << " ret " << (int)nesto2->read(5, buffer) << endl;
	cout << buffer << endl << endl;

	//check delete
	delete nesto2;
	cout << "Close file " << path2 << endl << endl;

	FS::deleteFile(path2);
	cout << "delete file " << path2 << endl << endl;

	nesto2 = FS::open(path2, 'r');
	cout << "fetched file !" << nesto2 << endl << endl;

	nesto2 = FS::open(path, 'w');
	cout << "fetched file !" << endl << endl;

	buffer[0] = 'h';
	buffer[1] = 'e';
	buffer[2] = 'r';
	for (int i = 0; i < 200*ClusterSize; i++)
		nesto2->write(3, buffer);

	delete nesto2;

	nesto2 = FS::open(path, 'r');


	char * b = new char[1000000];

	nesto2->read(1000000, b);

	delete nesto2;

	//cout << b << endl;

	for (char i = 'a'; i <= 'z'; i++)
	{
		for (char j = 'a'; j <= 'z'; j++)
		{
			path[4] = i;
			path[5] = j;
			File *n = FS::open(path, 'w');
			delete n;
		}
	}

	numEntries = FS::readRootDir(part1Ch, 0, dir);

	cout << "Found " << numEntries << " entries" << endl;
	for (int i = 0; i < numEntries; i++)
		cout << dir[i].getFullName() << endl;
	cout << endl;

	numEntries = FS::readRootDir(part1Ch, 100, dir);

	cout << "Found " << numEntries << " entries" << endl;
	for (int i = 0; i < numEntries; i++)
		cout << dir[i].getFullName() << endl;
	cout << endl;

	for (char i = 'a'; i <= 'z'; i++)
	{
		for (char j = 'a'; j <= 'z'; j++)
		{
			path[4] = i;
			path[5] = j;
			FS::deleteFile(path);
		}
	}

	numEntries = FS::readRootDir(part1Ch, 0, dir);

	cout << "Found " << numEntries << " entries" << endl;
	for (int i = 0; i < numEntries; i++)
		cout << dir[i].getFullName() << endl;
	cout << endl;


	return 0;
}