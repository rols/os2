#include "seek.h"

#include <cstring>


using namespace std;

Seek::Seek(char *buf)
{
	buffer = buf;
}

char Seek::readChar()
{
	if (end())
	{
		// ou maj gad
		return 1;
	}
	return buffer[cursor++];
}

unsigned long Seek::readULong()
{
	unsigned long ret;
	memcpy(&ret, buffer + cursor, 4);
	cursor += 4;

	return ret;
}

bool Seek::zero()
{
	//cout << "for buffer " << buffer << " and cursor " << cursor << " returns " << (buffer[cursor] == 0) << endl;
	if (cursor + 4 < ClusterSize)
	{
		for (int i = 0; i < 4; i++)
			if (buffer[cursor + i] != 0)
				return false;
	}
	
	return true;
}

bool Seek::end()
{
	return cursor >= ClusterSize;
}

ClusterNo Seek::pos()
{
	return cursor;
}

void Seek::moveCursor(int position)
{
	cursor = position;
}

void Seek::writeChar(char temp)
{
	buffer[cursor++] = temp;
}

void Seek::writeULong(unsigned long temp)
{
	memcpy(buffer + cursor, &temp, 4);
	cursor += 4;
}


Seek& operator>>(Seek& seek, Entry& entry)
{
	//double check this !!!
	if (seek.zero())
	{
		//fi.valid = false;
		return seek;
	}

	//fi.valid = true;

	for (int i = 0; i < 8; i++)
	{
		char temp = seek.readChar();
		entry.name[i] = temp;
	}

	for (int i = 0; i < 3; i++)
	{
		char temp = seek.readChar();
		entry.ext[i] = temp;
	}

	//fi.fullName = string(fi.name) + "." + string(fi.extension);

	entry.reserved = seek.readChar();

	entry.indexCluster = seek.readULong();

	entry.size = seek.readULong();

	return seek;
}

Seek& operator<<(Seek& seek, Entry& entry)
{
	for (int i = 0; i < 8; i++)
	{
		seek.writeChar(entry.name[i]);
	}

	for (int i = 0; i < 3; i++)
	{
		seek.writeChar(entry.ext[i]);
	}

	seek.writeChar(entry.reserved);

	seek.writeULong(entry.indexCluster);

	seek.writeULong(entry.size);

	return seek;
}