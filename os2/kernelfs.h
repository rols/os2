#pragma once

#include "logger.h"

#include "part.h"
#include "file.h"
#include "kernelpart.h"

#include <Windows.h>
#include <windows.h>
#include <cstdio>
#include <string>

#define NO_MOUNT_SPACE '0'

using namespace std;


class KernelFS {
public:
	~KernelFS();

	void lock();
	void unlock();

	char mount(Partition* partition); //montira particiju
											 // vraca dodeljeno slovo
	char unmount(char part); //demontira particiju oznacenu datim
									// slovom vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	char format(char part); //particija zadatu slovom se formatira;
								   // vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	char readRootDir(char part, EntryNum n, Entry (&d)[ENTRYCNT]);
	//prvim argumentom se zadaje particija, drugim redni broj
	//validnog ulaza od kog se pocinje citanje
	char doesExist(char* fname); //argument je naziv fajla sa
										//apsolutnom putanjom
	File* open(char* fname, char mode);
	char deleteFile(char* fname);

	KernelFS();

protected:
	static string mountSymbols; 
	KernelPartition **mountedPartitions;
	HANDLE mutex;

	friend class KernelFile;

};