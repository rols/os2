#include "kernelfs.h"

#include <iostream>

#define DEBUG_ROLS 0

string KernelFS::mountSymbols = "abcdefghijklmnopqrstuvwxyz";

KernelFS::KernelFS()
{
	//if (DEBUG_ROLS) Logger::log("  KernelFS constructor");

	//why doesnt static initializer work ? 
	mountSymbols = "abcdefghijklmnopqrstuvwxyz";

	int mountSize = mountSymbols.length();
	mountedPartitions = new KernelPartition*[mountSize];

	for (int i = 0; i < mountSize; i++)
	{
		mountedPartitions[i] = nullptr;
	}

	mutex = CreateSemaphore(NULL, 1, 10000, NULL);
	if (mutex == NULL)
	{
		//logError("CreateMutex error: %d\n", GetLastError());
		//return 1;
	}
}


KernelFS::~KernelFS() 
{
	if (DEBUG_ROLS) Logger::log("  KernelFS destructor");

	// first unmount all partitions ? 
	delete[] mountedPartitions;

	CloseHandle(mutex);
};


void KernelFS::lock()
{
	if (DEBUG_ROLS) Logger::log("  KernelFS lock");

	DWORD dwWaitResult = WaitForSingleObject(
		mutex,    // handle to mutex
		INFINITE);  // no time-out interval

	if (DEBUG_ROLS) Logger::log("  KernelFS locked");
}


void KernelFS::unlock()
{
	if (DEBUG_ROLS) Logger::log("  KernelFS unlock");

	if (!ReleaseSemaphore(mutex,1,0))
	{
		// Handle error.
	}
}


char KernelFS::mount(Partition* partition)
{
	if (DEBUG_ROLS) Logger::log("  KernelFS mount");

	int mountNum = mountSymbols.length();
	int ind;
	for (ind = 0; ind < mountNum; ind++)
	{
		if (mountedPartitions[ind] == nullptr)
			break;
	}

	if (ind == mountNum)
		return NO_MOUNT_SPACE;

	mountedPartitions[ind] = new KernelPartition(partition);

	return mountSymbols[ind];
}


char KernelFS::unmount(char part)
{
	if (DEBUG_ROLS) Logger::log("  KernelFS unmount " + part);

	int ind = mountSymbols.find(part);
	//waitAllFiles(ind);
	delete mountedPartitions[ind];
	mountedPartitions[ind] = nullptr;

	return 'c';
}


char KernelFS::format(char part)
{
	if (DEBUG_ROLS) Logger::log("  KernelFS format " + part);

	int ind = mountSymbols.find(part);
	//waitAllFiles(ind);

	mountedPartitions[ind]->format();

	return 1;
}


char KernelFS::doesExist(char *fname)
{
	if (DEBUG_ROLS) Logger::log("  KernelFS doesExist " + fname[0]);

	KernelPartition *currPartition = mountedPartitions[mountSymbols.find(fname[0])];

	if (currPartition == nullptr)
	{
		if (DEBUG_ROLS) Logger::log("Error, partition not mounted!");
		return 0;
	}

	string fileName = string(fname).substr(3);
	return currPartition->doesExist(fileName) ? 1 : 0;
}


char KernelFS::deleteFile(char *fname)
{
	if (DEBUG_ROLS) Logger::log("  KernelFS deletefile " + fname[0]);

	KernelPartition *currPartition = mountedPartitions[mountSymbols.find(fname[0])];

	if (currPartition == nullptr)
	{
		if (DEBUG_ROLS) Logger::log("Error, partition not mounted!");
		return 0;
	}

	string fileName = string(fname).substr(3);
	return currPartition->deleteFile(fileName);
}


File* KernelFS::open(char* fname, char mode)
{
	if (DEBUG_ROLS) Logger::log("  KernelFS open " + fname[0]);

	KernelPartition *currPartition = mountedPartitions[mountSymbols.find(fname[0])];

	if (currPartition == nullptr)
	{
		if (DEBUG_ROLS) Logger::log("Error, partition not mounted!");
		return 0;
	}

	string fileName = string(fname).substr(3);
	return currPartition->openFile(fileName, mode);
}


char KernelFS::readRootDir(char part, EntryNum n, Entry(&d)[ENTRYCNT])
{
	if (DEBUG_ROLS) Logger::log("  KernelFS readrootdir " + part);

	KernelPartition *currPartition = mountedPartitions[mountSymbols.find(part)];
	
	if (currPartition == nullptr)
	{
		if (DEBUG_ROLS) Logger::log("Error, partition not mounted!");
		return 0;
	}

	return currPartition->readRootDir(n, d);
}