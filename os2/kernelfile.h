#pragma once
#include <Windows.h>

#include "logger.h"

#include "fs.h"
#include "kernelfs.h"
#include "file.h"
#include "kernelpart.h"
#include "seek.h"

#include <vector>
#include <unordered_map>
#include <unordered_set>

const unsigned long FIRST_LEVEL_INDEX = (ClusterSize / 2 );
const unsigned long ENTRYSIZE = sizeof(unsigned long);
class KernelFile
{
public:
	KernelFile(string fileName, bool, KernelPartition*, ClusterNo, unsigned long);
	~KernelFile(); //zatvaranje fajla
	char write(BytesCnt, char* buffer);
	BytesCnt read(BytesCnt, char* buffer);
	char seek(BytesCnt);
	BytesCnt filePos();
	char eof();
	BytesCnt getFileSize();
	char truncate();

	void lock();
	void unlock();

private:
	string name;
	bool writePermission;
	HANDLE mutex;
	KernelPartition* kernelPartition;
	
	bool validCurrentCluster;
	ClusterNo currentCluster;
	char * currentBuffer;

	unsigned long currentSize;

	unsigned long cursor;

	//char *indexBuffer;
	ClusterNo indexCluster;
	void fillClustersFromIndex();
	void fillIndexFromClusters();
	vector<ClusterNo> fileClusters;
	vector<ClusterNo> secondLevelIndexes;
	void loadSecondLevelIndex(ClusterNo);
	void storeSecondLevelIndex(ClusterNo);

	//unordered_map<ClusterNo, char*> clustersCache;
	unordered_set<ClusterNo> clustersDirty;
	//unordered_set<ClusterNo> secondLevelClustersDirty;
	unordered_map<ClusterNo, vector<ClusterNo>*> secondLevelIndexClusterCache;
	//unordered_map<ClusterNo, char*> secondLevelIndexBuffer;
	//unordered_set<ClusterNo> secondLevelIndexDirty;

	ClusterNo getClusterFromPosition(unsigned long);
};