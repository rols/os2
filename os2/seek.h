#pragma once

#include "logger.h"

#include "part.h"
#include "fs.h"

class Seek
{
	int cursor;
	char *buffer;
public:
	Seek(char *buf);

	char readChar();
	unsigned long readULong();
	void writeChar(char);
	void writeULong(unsigned long);
	//bool eof();
	bool zero();
	bool end();
	ClusterNo pos();

	void moveCursor(int position);

};

Seek& operator<<(Seek& seek, Entry& entry);
Seek& operator>>(Seek& seek, Entry& entry);