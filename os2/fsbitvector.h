#pragma once

#include "logger.h"
#include "part.h"
#define NO_FREE_CLUSTERS -1

class FSBitVector
{
	char *buffer;
	int clusterNumber;

public:
	FSBitVector(char *, int);

	void setBit(ClusterNo, bool);

	bool isFree(ClusterNo);

	int findFree();
};