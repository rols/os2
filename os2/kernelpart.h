#pragma once

#include "logger.h"

#include "part.h"
#include "seek.h"
#include "file.h"
#include "fsbitvector.h"
#include <unordered_map>
#include <unordered_set>

#define DEEP_FORMAT false

using namespace std;


const unsigned long FILE_ENTRY_SIZE = 20;
class KernelPartition
{
public:
	KernelPartition(Partition *partition);
	~KernelPartition();

	void format();

	bool doesExist(string fileName);

	int readRootDir(EntryNum n, Entry(&d)[ENTRYCNT]);

	File * openFile(string fileName, char mode);

	bool deleteFile(string fileName);

	

protected:

	friend class KernelFile;

	unordered_map<ClusterNo, char*> cache;
	char * getCache(ClusterNo);
	char * getEmptyCache(ClusterNo);
	void persistCache(ClusterNo, bool drop = true);
	void dropCache(ClusterNo);
	//ordered map
	unordered_map<string, Entry*> files;
	vector<Entry*> filesVector;

	vector<ClusterNo> secondLevelRootIndex;

	//damn, ugly.
	unordered_map<string, int> fileReadingCount;
	unordered_map<string, HANDLE> fileReadingMutex;
	unordered_map<string, HANDLE> fileWritingMutex;
	//unordered_map<string, HANDLE> fileServiceMutex;
	unordered_map<string, int> fileWaiting;

	void initFirstCluster();
	
	void deepEraseAll();
	void deepEraseFile(int fileInd);
	
	//void eraseCacheCluster(int clusterNumber);
	//char * fetchCluster(ClusterNo clusterNumber);
	void releaseCluster(ClusterNo clusterNumber);
	//void fetchDummyCluster(ClusterNo clusterNumber);
	//void persistCluster(ClusterNo clusterNumber);
	void readRootIndex();
	void writeRootIndex();
	
	//void loadFile(string);

	int findAndTakeFreeCluster();
	
	File *openFileRead(string);
	File *openFileWrite(string);
	File *openFileReadWrite(string);
	File *openFileExisting(string, bool);
	File *openFileNew(string);
	void closeFile(string fileName, bool writePermission);
	void setNewFileSize(string, unsigned long);

	void fileWriterStart(string);
	void fileReaderStart(string);


	Partition *myPartition;

	int numberOfClusters;

	//int currentClusterNumber;
	int rootDirIndexStart;
	//char buffer[ClusterSize];

	void waitAllFiles();

	bool lockPartition = false;
};