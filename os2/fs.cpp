#include "fs.h"
#include "kernelfs.h"

KernelFS* FS::myImpl = new KernelFS();

FS::FS() {};
FS::~FS() {};

char FS::mount(Partition* partition) //montira particiju, vraca dodeljeno slovo
{
	myImpl->lock();
	char val = myImpl->mount(partition);
	myImpl->unlock();
	return val;
}


char FS::unmount(char part)
{
	myImpl->lock();
	char val = myImpl->unmount(part);
	myImpl->unlock();
	return val;
}

char FS::format(char part)
{
	myImpl->lock();
	char val = myImpl->format(part);
	myImpl->unlock();
	return val;
}

char FS::readRootDir(char part, EntryNum entryNum, Entry (&directory)[ENTRYCNT])
{
	myImpl->lock();
	char val = myImpl->readRootDir(part, entryNum, directory);
	myImpl->unlock();
	return val;
}

char FS::doesExist(char *fname)
{
	myImpl->lock();
	char val = myImpl->doesExist(fname);
	myImpl->unlock();
	return val;
}

char FS::deleteFile(char *fname)
{
	myImpl->lock();
	char val = myImpl->deleteFile(fname);
	myImpl->unlock();
	return val;
}

File* FS::open(char* fname, char mode)
{
	myImpl->lock();
	File *val = myImpl->open(fname, mode);
	myImpl->unlock();
	return val;
}


string Entry::getFullName()
{
	return string(name) + "." + string(ext);
}

void Entry::setFullName(string fullName)
{
	int dot = fullName.find('.');
	memcpy(name, fullName.data(), dot);
	memcpy(ext, fullName.substr(dot + 1).data(), fullName.length() - dot - 1);
}