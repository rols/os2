#include "kernelpart.h"

#include <cstring>

#define DEBUG_ROLS 0

using namespace std;

KernelPartition::KernelPartition(Partition *partition)
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition constructor");

	myPartition = partition;
	numberOfClusters = partition->getNumOfClusters();
	//bits in byte
	rootDirIndexStart = numberOfClusters / 8;

}


KernelPartition::~KernelPartition()
{
	waitAllFiles();

	//something else maybe ?delete everything
}


void KernelPartition::format()
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition format");

	waitAllFiles();

	if (DEBUG_ROLS) Logger::log(" finished waiting for files");

	if (DEEP_FORMAT)
		deepEraseAll();

	initFirstCluster();

	lockPartition = false;

	cache.clear();
}

void KernelPartition::initFirstCluster()
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition initFirstCluster");

	//fetchDummyCluster(0);
	//eraseCacheCluster(0);
	char *buf = getCache(0);

	memset(buf, 0, ClusterSize);

	FSBitVector fsBit(buf, numberOfClusters);
	fsBit.setBit(0, true);

	//ClusterNo clusterNumber = myPartition->getNumOfClusters();
	//myPartition->writeCluster(currentClusterNumber, buffer);
	persistCache(0, false);
}


void KernelPartition::releaseCluster(ClusterNo clusterNumber) 
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition releaseCluster");

	//fetchCluster(0);
	char *buf = getCache(0);

	FSBitVector fsbit(buf, clusterNumber);

	fsbit.setBit(clusterNumber, false);
	dropCache(clusterNumber);

	persistCache(0);
}



bool KernelPartition::doesExist(string fileName)
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition doesExist");

	readRootIndex();

	return files.find(fileName) != files.end();

}

void KernelPartition::readRootIndex()
{
	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}

	if (DEBUG_ROLS) Logger::log("    KernelPartition readRootIndex");

	//much ulgy, many must change.
	//if (cache.find(0) != cache.end())
	//	return;

	//fetchCluster(0);
	char *buf = getCache(0);

	//optimize
	files.clear();
	filesVector.clear();
	secondLevelRootIndex.clear();

	Seek seek(buf);
	seek.moveCursor(rootDirIndexStart);

	//cout << "created seek" << endl;

	while (!seek.zero())
	{
		if (seek.pos() < FIRST_LEVEL_INDEX)
		{
			Entry *entry = new Entry();

			seek >> *entry;
			files[entry->getFullName()] = entry;
			filesVector.push_back(entry);
			if (DEBUG_ROLS) Logger::log(" fileinfo while loop " + entry->getFullName());

			if (seek.pos() < FIRST_LEVEL_INDEX && seek.pos() + FILE_ENTRY_SIZE >= FIRST_LEVEL_INDEX) //lomiiiiiiiiiiii
			{
				seek.moveCursor(FIRST_LEVEL_INDEX);
			}
		}
		else
		{
			ClusterNo secondLevel = seek.readULong();
			secondLevelRootIndex.push_back(secondLevel);

			if (DEBUG_ROLS) Logger::log(" fileinfo while loop secondlevel cluster " + secondLevel);

			char *bufSecond = getCache(secondLevel);
			Seek seekSecond(bufSecond);
			seekSecond.moveCursor(0);

			//this shouldnt be here
			while (!seekSecond.zero() && seekSecond.pos() + FILE_ENTRY_SIZE < ClusterSize)
			{
				Entry *entry = new Entry();

				seekSecond >> *entry;
				files[entry->getFullName()] = entry;
				filesVector.push_back(entry);
				if (DEBUG_ROLS) Logger::log(" fileinfo while loop secondlevel "+ entry->getFullName());
			}
		}

	}
	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}
}

void KernelPartition::writeRootIndex()
{

	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}
	if (DEBUG_ROLS) Logger::log("    KernelPartition writeRootIndex");

	//if (cache.find(0) == cache.end())
	//{
	//	cout << "Not in cache !" << endl;
	//}

	int secondLevelI = -1;

	char *buf = getCache(0);
	char *secondLevelBuf = nullptr;

	Seek *secondLevelSeek = nullptr;

	Seek seek(buf);
	seek.moveCursor(rootDirIndexStart);

	//for (pair<string, Entry*> entryPair : files)
	for (Entry *entry : filesVector)
	{
		if (DEBUG_ROLS) Logger::log(" entries write loop " + entry->getFullName());

		if (seek.pos() + FILE_ENTRY_SIZE < FIRST_LEVEL_INDEX)
		{
			seek << *(entry);
		}
		else
		{
			if (seek.pos() < FIRST_LEVEL_INDEX)
			{
				seek.moveCursor(FIRST_LEVEL_INDEX);
			}

			if (secondLevelBuf == nullptr || secondLevelSeek->pos() + FILE_ENTRY_SIZE >= ClusterSize)
			{
				if (secondLevelSeek != nullptr)
				{
					while (!secondLevelSeek->end()) secondLevelSeek->writeChar(0);
					delete secondLevelSeek;
					persistCache(secondLevelRootIndex[secondLevelI], false);
				}


				secondLevelI++;

				if (secondLevelI >= secondLevelRootIndex.size())
				{
					if (seek.pos() + 4 < seek.end())
					{
						if (DEBUG_ROLS) Logger::log(" No free space ! root index full");
						return;
					}

					int freeCluster = findAndTakeFreeCluster();
					if (freeCluster == NO_FREE_CLUSTERS)
					{
						if (DEBUG_ROLS) Logger::log(" No free space !");
						return;
					}

					secondLevelRootIndex.push_back(freeCluster);
					secondLevelBuf = getEmptyCache(secondLevelRootIndex[secondLevelI]);
				}

				else 
				{
					secondLevelBuf = getCache(secondLevelRootIndex[secondLevelI]);
				}

				secondLevelSeek = new Seek(secondLevelBuf);
				secondLevelSeek->moveCursor(0);

				seek.writeULong(secondLevelRootIndex[secondLevelI]);
			}

			*secondLevelSeek << *entry;
		}

	}

	if (secondLevelSeek != nullptr)
	{
		while (!secondLevelSeek->end()) secondLevelSeek->writeChar(0);
		delete secondLevelSeek;
		persistCache(secondLevelRootIndex[secondLevelI], false);
	}

	FSBitVector fsbit(buf, numberOfClusters);
	if (secondLevelI + 1 < secondLevelRootIndex.size())
	{
		secondLevelI++;

		while (secondLevelI < secondLevelRootIndex.size())
		{
			fsbit.setBit(secondLevelRootIndex[secondLevelI], false);
			dropCache(secondLevelRootIndex[secondLevelI]);
			secondLevelI++;
		}
	}

	while (!seek.end()) seek.writeChar(0);

	//filesVector.clear();
	//files.clear();

	//ou maj gad, some heavy shit!
	persistCache(0, false);
	//readRootIndex();

	readRootIndex();

	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}
}

void KernelPartition::deepEraseAll()
{

}

void KernelPartition::deepEraseFile(int fileInd)
{

}

File * KernelPartition::openFile(string fileName, char mode)
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition open " + fileName + " mode " + mode);
	
	if (lockPartition)
	{
		if (DEBUG_ROLS) Logger::log(" partition is locked ! (for formating, or mounting)");
		return nullptr;
	}

	readRootIndex();

	//if doesnt exist, if write mode...

	File * ret = nullptr;

	switch (mode)
	{
	case 'r': ret = openFileRead(fileName); break;
	case 'w': ret = openFileWrite(fileName); break;
	case 'a': ret = openFileReadWrite(fileName); break;
	}

	return ret;
}


File * KernelPartition::openFileRead(string fileName)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition openFileRead");

	if (!doesExist(fileName))
	{
		if (DEBUG_ROLS) Logger::log("  no such file !");
		return nullptr;
	}


	fileReaderStart(fileName);

	File * ret = openFileExisting(fileName, false);

	return ret;
}


File * KernelPartition::openFileWrite(string fileName)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition openFileWrite");

	fileWriterStart(fileName);

	deleteFile(fileName);

	File * ret = openFileNew(fileName);

	return ret;
}


File * KernelPartition::openFileReadWrite(string fileName)
{
	
	if (DEBUG_ROLS) Logger::log( "    KernelPartition openFileReadWrite");

	File * ret;

	fileWriterStart(fileName);

	if (doesExist(fileName))
	{
		ret = openFileExisting(fileName, true);
		ret->myImpl->seek(ret->myImpl->getFileSize());
	}

	else
	{
		ret = openFileNew(fileName);
	}

	return ret;
}

File * KernelPartition::openFileExisting(string fileName, bool writePermission)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition openFileExisting");

	//open File
	if (writePermission)
	{
		//fileWriterStart(fileName);
	}
	else
	{
		//fileReaderStart(fileName);
	}

	//readRootIndex();
	ClusterNo index = files[fileName]->indexCluster;

	//fetchCluster(index); 
	char *buf = getCache(index);

	File *ret = new File();
	ret->myImpl = new KernelFile(fileName, writePermission, this, index, files[fileName]->size);
	return ret;
}

File * KernelPartition::openFileNew(string fileName)
{
	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}

	if (DEBUG_ROLS) Logger::log( "    KernelPartition openFileNew");

	Entry *entry = new Entry();

	int freeCluster = findAndTakeFreeCluster();
	if (freeCluster == NO_FREE_CLUSTERS)
	{
		if (DEBUG_ROLS) Logger::log( " No free space !");
		return nullptr;
	}

	//cout << "free cluster " << freeCluster << endl;

	entry->setFullName(fileName);
	entry->indexCluster = freeCluster;
	entry->size = 0;

	//fetchCluster(freeCluster);
	//eraseCacheCluster(freeCluster);
	char *buf = getEmptyCache(freeCluster);

	//cout << "fileinfo init" << endl;

	files[fileName] = entry;
	filesVector.push_back(entry);

	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}

	writeRootIndex();


	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}

	File *ret = new File();
	ret->myImpl = new KernelFile(fileName, true, this, freeCluster, 0);
	return ret;
}

bool KernelPartition::deleteFile(string fileName)
{
	if (lockPartition)
	{
		if (DEBUG_ROLS) Logger::log(" partition is locked ! (for formating, or mounting)");
		return false;
	}

	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}
	//delete everything !!!!
	if (DEBUG_ROLS) Logger::log( "    KernelPartition deleteFile");

	if (!doesExist(fileName))
	{
		if (DEBUG_ROLS) Logger::log( "  file doesn't exist ");
		return false;
	}

	readRootIndex();

	char *buf = getCache(0);
	
	FSBitVector fsbit(buf, numberOfClusters);

	if (files[fileName]->size > 0)
	{
		int fileIndex = files[fileName]->indexCluster;

		char *indexBuffer = getCache(fileIndex);

		Seek seek(indexBuffer);
		seek.moveCursor(0);

		while (true)
		{
			if (seek.end())
				break;

			ClusterNo currDataCluster = seek.readULong();

			if (currDataCluster == 0)
				break;

			if (seek.pos() <= FIRST_LEVEL_INDEX)
			{
				fsbit.setBit(currDataCluster, false);
				dropCache(currDataCluster);
			}
			else
			{
				char *secondLevelIndexBuffer = getCache(currDataCluster);

				Seek seekSecondLevel(secondLevelIndexBuffer);
				seekSecondLevel.moveCursor(0);

				while (true)
				{
					if (seekSecondLevel.end())
						break;

					ClusterNo currDataClusterSecondLevel = seekSecondLevel.readULong();

					if (currDataCluster == 0)
						break;

					fsbit.setBit(currDataClusterSecondLevel, false);
					dropCache(currDataClusterSecondLevel);
				}

				fsbit.setBit(currDataCluster, false);
				dropCache(currDataCluster);
			}

		}

		fsbit.setBit(fileIndex, false);
		dropCache(fileIndex);
	}


	files.erase(fileName);
	//whut ? oh shit
	//filesVector.erase(filename);
	
	vector<Entry*> second;
	for (Entry * ent : filesVector)
	{
		
		if (ent->getFullName() != fileName)
			second.push_back(ent);
	}

	filesVector.clear();
	for (Entry * ent : second)
		filesVector.push_back(ent);
	if (files.size() != filesVector.size())
	{
		if (DEBUG_ROLS) Logger::log("WHOAA");
	}
	writeRootIndex();

	return true;
}



int KernelPartition::findAndTakeFreeCluster()
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition findandtakefreecluster");

	//fetchCluster(0);
	char *buf = getCache(0);
	
	FSBitVector fsbit(buf, numberOfClusters);

	int ret = fsbit.findFree();

	if (DEBUG_ROLS) Logger::log( " found free " + ret);

	if (ret != NO_FREE_CLUSTERS)
	{
		fsbit.setBit(ret, true);
	}

	return ret;
}


int KernelPartition::readRootDir(EntryNum entryNum, Entry(&directory)[ENTRYCNT])
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition readRootDir");

	readRootIndex();

	int fileInd;

	for (fileInd = 0; fileInd + entryNum < filesVector.size() && fileInd < ENTRYCNT; fileInd++)
	{
		directory[fileInd] = *(filesVector[fileInd + entryNum]);
	}

	if (filesVector.size() - entryNum != ENTRYCNT && fileInd == ENTRYCNT)
	{
		fileInd = ENTRYCNT_SUCCESS; //theres more
	}

	return fileInd;
}

void KernelPartition::fileWriterStart(string fileName)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition fileWriterStart");

	if (fileWaiting.find(fileName) == fileWaiting.end())
	{
		fileWaiting[fileName] = 0;
		//fileServiceMutex[fileName] = CreateMutex(NULL, FALSE, NULL);
		fileReadingMutex[fileName] = CreateSemaphore(NULL, 1, 10000, NULL);
		fileWritingMutex[fileName] = CreateSemaphore(NULL, 1, 10000, NULL);
		fileReadingCount[fileName] = 0;
	}

	fileWaiting[fileName] ++;

	//HANDLE serviceQueue = fileServiceMutex[fileName];
	HANDLE resourceAccess = fileWritingMutex[fileName];
	HANDLE readCountAccess = fileReadingMutex[fileName];

	//others might do something while this opens files
	FS::myImpl->unlock();

	DWORD dwWaitResult = WaitForSingleObject(resourceAccess, INFINITE);

	FS::myImpl->lock();

}

void KernelPartition::fileReaderStart(string fileName)
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition fileReaderStart");

	if (fileWaiting.find(fileName) == fileWaiting.end())
	{
		fileWaiting[fileName] = 0;
		//fileServiceMutex[fileName] = CreateMutex(NULL, FALSE, NULL);
		fileReadingMutex[fileName] = CreateSemaphore(NULL, 1, 10000, NULL);
		fileWritingMutex[fileName] = CreateSemaphore(NULL, 1, 10000, NULL);
		fileReadingCount[fileName] = 0;
	}

	fileWaiting[fileName] ++;

	//HANDLE serviceQueue = fileServiceMutex[fileName];
	HANDLE resourceAccess = fileWritingMutex[fileName];
	HANDLE readCountAccess = fileReadingMutex[fileName];
	
	//others might do something while this opens files
	FS::myImpl->unlock();

	DWORD dwWaitResult = WaitForSingleObject(readCountAccess, INFINITE);
	if (fileReadingCount[fileName] == 0)
	{
		DWORD dwWaitResult2 = WaitForSingleObject(resourceAccess, INFINITE);
	}
	fileReadingCount[fileName]++;
	ReleaseSemaphore(readCountAccess,1,NULL);

	FS::myImpl->lock();
}

void KernelPartition::closeFile(string fileName, bool writePermission)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition closeFile");

	HANDLE resourceAccess = fileWritingMutex[fileName];
	HANDLE readCountAccess = fileReadingMutex[fileName];

	fileWaiting[fileName]--;

	if (writePermission)
	{
		ReleaseSemaphore(resourceAccess,1,NULL);
	}
	else
	{
		DWORD dwWaitResult = WaitForSingleObject(readCountAccess, INFINITE);
		fileReadingCount[fileName]--;
		if (fileReadingCount[fileName] == 0)
		{
			ReleaseSemaphore(resourceAccess,1,NULL);
		}
		ReleaseSemaphore(readCountAccess,1,NULL);
	}

	if (fileWaiting[fileName] == 0)
	{
		fileWaiting.erase(fileName);
		CloseHandle(fileReadingMutex[fileName]);
		fileReadingMutex.erase(fileName);
		CloseHandle(fileWritingMutex[fileName]);
		fileWritingMutex.erase(fileName);
		fileReadingCount.erase(fileName);
	}
}


void KernelPartition::waitAllFiles()
{
	lockPartition = true;

	readRootIndex();

	//diff between vector and hashmap
	if (files.size() == filesVector.size())
	{
		vector<string> filesVectorTemp;
		for (Entry *e : filesVector)
			filesVectorTemp.push_back(e->getFullName());

		FS::myImpl->unlock();

		for (string fileName : filesVectorTemp)
		{
			//nothing to be done, none of the process are waiting
			if (fileWritingMutex.find(fileName) == fileWritingMutex.end())
				continue;

			HANDLE resourceAccess = fileWritingMutex[fileName];
			DWORD dwWaitResult = WaitForSingleObject(resourceAccess, INFINITE);

			//but what if its closed ?

			while (fileWaiting[fileName] != 0)
			{
				ReleaseSemaphore(resourceAccess,1,NULL);
				DWORD dwWaitResult = WaitForSingleObject(resourceAccess, INFINITE);
			}

			ReleaseSemaphore(resourceAccess,1,NULL);
		}

		FS::myImpl->lock();
	}
}

void KernelPartition::setNewFileSize(string fileName, unsigned long newSize)
{

	if (DEBUG_ROLS) Logger::log( "    KernelPartition setnewfilesize");
	readRootIndex();

	files[fileName]->size = newSize;

	writeRootIndex();
}


char * KernelPartition::getCache(ClusterNo clusterNumber)
{
	if (DEBUG_ROLS) Logger::log("    KernelPartition getcache");
	if (cache.find(clusterNumber) == cache.end())
	{
		cache[clusterNumber] = new char[2048];
		myPartition->readCluster(clusterNumber, cache[clusterNumber]);

		char *buf = new char[ClusterSize];
		memcpy(buf, cache[clusterNumber], ClusterSize);
		delete cache[clusterNumber];
		cache[clusterNumber] = buf;

		if (DEBUG_ROLS) Logger::log( " Fetched cluster " + clusterNumber);
	}
	else
	{
		if (DEBUG_ROLS) Logger::log( " Cluster already in memory " + clusterNumber);
	}

	return cache[clusterNumber];
}

char * KernelPartition::getEmptyCache(ClusterNo clusterNumber)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition getemptycache");

	if (cache.find(clusterNumber) == cache.end())
	{
		cache[clusterNumber] = new char[ClusterSize];
		if (DEBUG_ROLS) Logger::log( " Initialized dummy cluster " + clusterNumber);
		memset(cache[clusterNumber], 0, ClusterSize);
		return cache[clusterNumber];
	}
	else
	{
		if (DEBUG_ROLS) Logger::log( " Cluster  already in memory ! ERRNO " + clusterNumber);
		return nullptr;
	}
}

void KernelPartition::persistCache(ClusterNo clusterNumber, bool drop)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition persistCluster");

	if (cache.find(clusterNumber) == cache.end())
	{
		if (DEBUG_ROLS) Logger::log( " Cluster not in memory!" + clusterNumber);
	}

	else
	{
		if (DEBUG_ROLS) Logger::log( "" + cache[clusterNumber][0] + cache[clusterNumber][125]);
		myPartition->writeCluster(clusterNumber, cache[clusterNumber]);
		//cache.erase(clusterNumber);
		if (drop)
			dropCache(clusterNumber);

		if (DEBUG_ROLS) Logger::log( " Persisted cluster " + clusterNumber);
	}
}

void KernelPartition::dropCache(ClusterNo clusterNumber)
{
	if (DEBUG_ROLS) Logger::log( "    KernelPartition dropCluster");

	if (cache.find(clusterNumber) == cache.end())
	{
		if (DEBUG_ROLS) Logger::log( " Cluster not in mem " + clusterNumber);
	}

	else
	{
		cache.erase(clusterNumber);

		if (DEBUG_ROLS) Logger::log( " Erased cluster " + clusterNumber);
	}
}