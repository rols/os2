#include "fsbitvector.h"

#include <iostream>

#define DEBUG_ROLS 0

using namespace std;

FSBitVector::FSBitVector(char *buf, int cn)
{
	buffer = buf;
	clusterNumber = cn;
}

void FSBitVector::setBit(ClusterNo clusterNo, bool used)
{
	if (used)
		buffer[clusterNo >> 3] |= ( 1 << (7 - (clusterNo & 7)));
	else
		buffer[clusterNo >> 3] &= ~(1 << (7 - (clusterNo & 7)));
}

bool FSBitVector::isFree(ClusterNo clusterNo)
{
	return buffer[clusterNo / 8] & (1 << (7 - (clusterNo & 7))) == 0;
}

int FSBitVector::findFree()
{
	if (DEBUG_ROLS) Logger::log("Bitvector findFree");

	int clusterInd;
	for (clusterInd = 0; clusterInd < clusterNumber / 8 && (unsigned char)buffer[clusterInd] == 255; clusterInd++);

	if (clusterInd == clusterNumber / 8)
		return NO_FREE_CLUSTERS;

	unsigned char temp = ~(unsigned char)buffer[clusterInd];

	clusterInd <<= 3;
	clusterInd += 8;


	while (temp != 0)
	{
		temp >>= 1;
		clusterInd--;
	}

	return clusterInd;
}
